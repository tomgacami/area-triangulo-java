package Educacion;

public class TiposVariables {

    public static void main(String[] args){

       String miNom = "Chelin";
       System.out.println("mi nombre es " + miNom);
       miNom = "Juan";
        System.out.println("mi nombre es " + miNom);

        boolean estaProgramando = true;
        System.out.println(estaProgramando);

        Float num1 = (float)25, num2 = 13f, resultado;
        System.out.println("La suma da: " + (num1+num2) );
        System.out.println("La resta da: " + (num1-num2) );
        System.out.println("La division da: " + (num1/num2) );

        System.out.println("La division entre cero da: " + (num1/0) );
        System.out.println("Y AHORA...?" );


    }
}
