package Calculo;

import Exepciones.NuevasExcepciones;

public class CalculoDeAristas {

    public Integer calculoDeAristas( Integer cantVertices) throws NuevasExcepciones {

        if( cantVertices == 0) {
            throw new NuevasExcepciones("El valor de vertices ingresado es cero");
        } else if (cantVertices < 0 ){
            throw new NuevasExcepciones("El valor de vertices ingresado es menor a cero");
        }
        return ((cantVertices*(cantVertices-1))/2) ;
    }
}
