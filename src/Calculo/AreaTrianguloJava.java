package Calculo;

import Exepciones.NuevasExcepciones;
import java.lang.Math;

public class AreaTrianguloJava {

    public Integer calcularPorBaseAltura( Integer altura, Integer base) throws NuevasExcepciones {

        if( base <= 0){
            throw new NuevasExcepciones ("El dato de la base ingresado es menor que 1");
        }else if (altura <=0 ){
            throw new  NuevasExcepciones ("El dato de la altura ingresado es menor que 1");
        }
        return (base*altura)/2;
    }

    public Integer calcularIngresandoLados( Integer ladoCorto, Integer ladoLargo, Integer tercerLado) throws NuevasExcepciones{

        if( ladoCorto <= 0){
            throw new NuevasExcepciones ("El lado mas corto es menor que 0");
        }else  if( ladoLargo <=0 ){
            throw new NuevasExcepciones ("El lado mas largo es menor que 0");
        }else if( tercerLado <= 0){
            throw new NuevasExcepciones ("El tercer lado es menor que 0");
        }else if( Math.sqrt((Math.pow(ladoCorto,2)+ Math.pow(ladoLargo, 2))) <= tercerLado){
            throw new NuevasExcepciones("El triangulo no es cerrado");
        }

        return (ladoCorto*ladoLargo)/2;
    }
}
