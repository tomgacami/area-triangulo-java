
import Calculo.AreaTrianguloJava;
import Calculo.CalculoDeAristas;
import Exepciones.NuevasExcepciones;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int numero1 = 25;
        System.out.println("La variable numero1 vale: " + numero1);
        System.out.println(numero1);
        String nom = "cehil";
        System.out.println("mi nombre es: " + nom);
        nom = "Juan";
        System.out.println("mi nuevo nombre es: " + nom);

        System.out.print("Ingrese un numero para imprimir: ");

//        Scanner myVar = new Scanner(System.in);
//        System.out.println("Hello World! ahora a meter mano + " + myVar.nextInt());

        System.out.print("\n");
        System.out.println("AHORA EMPEZA EL METODO CON LA CLASE AREA TRIANGULO");

        System.out.print("\n");

        AreaTrianguloJava aTj = new AreaTrianguloJava();
        CalculoDeAristas calculoAristas = new CalculoDeAristas();

        int opcion, resultado;
        char opcionSalida;
        Scanner lector = new Scanner(System.in);

        System.out.println("BUENAS BUENAS JUGADOR");

        do {
            System.out.println("Ingrese la opcion");
            System.out.println("    1-Para ingresar base y altura");
            System.out.println("    2-Para ingresar la medida de los lados del triangulo");
            System.out.println("    3-Calcular cantidad de aristas");
            System.out.println("    4-Calcular cantidad de aristas de 1 vertice a un numero\n");
            System.out.print("Ingrese opcion: ");

            opcion = lector.nextInt();

            switch (opcion) {

                case 1:

                    int altura, base;
                    System.out.println("Ingrese la medida de los lados de triangulo:");
                    System.out.print("    Altura: ");
//                    altura = lector.nextInt();
                    if (lector.hasNextInt(altura = lector.nextInt())) {

//                        altura = lector.nextInt();

                        System.out.print("    Base: ");
                        base = lector.nextInt();

                        try {
                            resultado = aTj.calcularPorBaseAltura(altura, base);
                            System.out.println("El area del trangulo es: " + resultado);

                        } catch (NuevasExcepciones except) {
                            System.out.println(except.getMessage());
                        }
                    }
                    break;

                case 2:

                    int ladoLargo, ladoCorto, tercerLado;
                    System.out.println("Ingrese las medidas de los lados: ");
                    System.out.print("    Ingrese el lado mas largo: ");
                    ladoLargo = lector.nextInt();
                    System.out.print("    Ingrese el lado mas corto: ");
                    ladoCorto = lector.nextInt();
                    System.out.print("    Ingrese el tercer lado: ");
                    tercerLado = lector.nextInt();

                    try {
                        resultado = aTj.calcularIngresandoLados(ladoCorto, ladoLargo, tercerLado);
                        System.out.println("El area del trangulo es: " + resultado);
                    } catch (NuevasExcepciones except) {
                        System.out.println(except.getMessage());
                    }
                    break;

                case 3:
                    int cantVertices;
                    System.out.print("Ingrese cantidad de vertices: ");
                    cantVertices = lector.nextInt();

                    try {
                        resultado = calculoAristas.calculoDeAristas(cantVertices);
                        System.out.println("La cantidad de aristas es: " + resultado);

                    } catch (NuevasExcepciones except) {
                        System.out.println(except.getMessage());
                    }

                    break;

                case 4:
                    int cantVerticeMax, resultadoAnterior = 0;
                    System.out.print("Ingrese cantidad de vertices: ");
                    cantVerticeMax = lector.nextInt();

                    if (cantVerticeMax > 0) {

                        System.out.print("\n");
                        for (int iterador = 1; iterador <= cantVerticeMax; iterador++) {

                            try {
                                resultado = calculoAristas.calculoDeAristas(iterador);
                                System.out.println(iterador + "v --> " + resultado + "a, diferencia con el anterior: " + (resultado - resultadoAnterior));
                                resultadoAnterior = resultado;

                            } catch (NuevasExcepciones except) {
                                System.out.println(except.getMessage());
                            }
                        }
                    } else if (cantVerticeMax == 0) {
                        System.out.println("El numero ingresado es cero. Ingrese un numero mayor");
                    } else if (cantVerticeMax < 0) {
                        System.out.println("El numero ingresado es menor que cero. Ingrese un numero mayor a cero");
                    }

//                        System.out.print("\n\n");
                    break;

                default:
                    System.out.println("No hay opcion disponible para valor ingresado");
            }

            System.out.print("\nQuiere seguir calculadno? S/n ");
            opcionSalida = lector.next().charAt(0);

            while (opcionSalida != 'S' && opcionSalida != 's' && opcionSalida != 'n') {

                System.out.print("Opcion no valida, vuelva a ingresar opcion: ");
                opcionSalida = lector.next().charAt(0);
            }
            System.out.println();
        } while (opcionSalida == 'S' || opcionSalida == 's');

        System.out.println("FIN DEL PROGRAMA!!!\n\n");
    }
}
